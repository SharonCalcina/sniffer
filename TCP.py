from socket import *
import textwrap
import struct
import sys
import re
import socket
import binascii

TAB_1 = '\t - '
TAB_2 = '\t\t - '
TAB_3 = '\t\t\t - '
TAB_4 = '\t\t\t\t - '

DATA_TAB_1 = '\t   '
DATA_TAB_2 = '\t\t   '
DATA_TAB_3 = '\t\t\t   '
DATA_TAB_4 = '\t\t\t\t   '
 
# receive a datagram
def receiveData(s):
    data = ''
    try:
        data = s.recvfrom(65565)
    except timeout:
        data = ''
    except:
        print ("An error happened: ")
        sys.exc_info()
    return data[0]

# Ethernet Header 
def ethernet_frame(data):
    dest_mac, src_mac, proto=struct.unpack('! 6s 6s H', data[:14])
    return get_mac_addr(dest_mac), get_mac_addr(src_mac),socket.htons(proto),data[14:]

# devuelve la direccion MAC propiamente formateada 
def get_mac_addr(bytes_addr):
    bytes_str=map('{:02x}'.format, bytes_addr)
    return ':'.join(bytes_str).upper()

# get Type of Service: 8 bits
def getTOS(data):
    precedence = {0: "Routine", 1: "Priority", 2: "Immediate", 3: "Flash", 4: "Flash override", 5: "CRITIC/ECP",
                  6: "Internetwork control", 7: "Network control"}
    delay = {0: "Normal delay", 1: "Low delay"}
    throughput = {0: "Normal throughput", 1: "High throughput"}
    reliability = {0: "Normal reliability", 1: "High reliability"}
    cost = {0: "Normal monetary cost", 1: "Minimize monetary cost"}
 
#   get the 3rd bit and shift right
    D = data & 0x10
    D >>= 4
#   get the 4th bit and shift right
    T = data & 0x8
    T >>= 3
#   get the 5th bit and shift right
    R = data & 0x4
    R >>= 2
#   get the 6th bit and shift right
    M = data & 0x2
    M >>= 1
#   the 7th bit is empty and shouldn't be analyzed
 
    tabs = '\n\t\t\t'
    TOS = precedence[data >> 5] + tabs + delay[D] + tabs + throughput[T] + tabs + \
            reliability[R] + tabs + cost[M]
    return TOS
 
# get Flags: 3 bits
def getFlags(data):
    flagR = {0: "0 - Reserved bit"}
    flagDF = {0: "0 - Fragment if necessary", 1: "1 - Do not fragment"}
    flagMF = {0: "0 - Last fragment", 1: "1 - More fragments"}
 
#   get the 1st bit and shift right
    R = data & 0x8000
    R >>= 15
#   get the 2nd bit and shift right
    DF = data & 0x4000
    DF >>= 14
#   get the 3rd bit and shift right
    MF = data & 0x2000
    MF >>= 13
 
    tabs = '\n\t\t\t'
    flags = flagR[R] + tabs + flagDF[DF] + tabs + flagMF[MF]
    return flags
 
# get protocol: 8 bits
def getProtocol(protocolNr):
    protocolFile = open('Protocol.txt', 'r')
    protocolData = protocolFile.read()
    protocol = re.findall(r'\n' + str(protocolNr) + ' (?:.)+\n', protocolData)
    if protocol:
        protocol = protocol[0]
        protocol = protocol.replace("\n", "")
        protocol = protocol.replace(str(protocolNr), "")
        protocol = protocol.lstrip()
        return protocol
 
    else:
        return protocolNr
#desempaquetar paquete ICMP
def icmp_packet(data):
    icmp_type,code,checksum = struct.unpack('! B B H', data[:4])
    return icmp_type,code,checksum, data[4:]

#para formatear data multi-linea
def format_multi_line(prefix, string, size=80):
    size-= len(prefix)
    if isinstance(string, bytes):
        string=''.join(r'\x{:02x}'.format(byte) for byte in string)
        if size % 2:
            size-=1
    return '\n'.join([prefix + line for line in textwrap.wrap(string, size)])
#desempaquetar segmento TCP
def tcp_segment(data):
    (src_port, dest_port, sequence, acknowledgement, offset_reserved_flags,window,checksum,urgent)= struct.unpack('! H H L L H H H H', data[:20])
    offset=(offset_reserved_flags >> 12)*4
    flags_urg=(offset_reserved_flags & 32) >> 5
    flags_ack=(offset_reserved_flags & 16) >> 4
    flags_psh=(offset_reserved_flags & 8) >> 3
    flags_rst=(offset_reserved_flags & 4) >> 2
    flags_syn=(offset_reserved_flags & 2) >> 1
    flags_fin=offset_reserved_flags & 1
    data_offset=offset_reserved_flags >> 4
    #offset=(offset_reserved_flags >> 12)*4
    #flags_urg=offset_reserved_flags & 0x20 
    #flags_urg>>=5
    #flags_ack=offset_reserved_flags & 0x10
    #flags_ack>>= 4
    #flags_psh=offset_reserved_flags & 0x8
    #flags_psh >>= 3
    #flags_rst=offset_reserved_flags & 0x4
    #flags_rst >>= 2
    #flags_syn=offset_reserved_flags & 0x2
    #flags_syn >>= 1
    #flags_fin=offset_reserved_flags & 0x0
    return src_port, dest_port, sequence, acknowledgement, flags_urg, flags_ack, flags_psh,flags_rst,flags_syn,flags_fin,data_offset,window,checksum,urgent,data[offset:]

# desempaquetar segmento UDP
def udp_segment(data):
    src_port, dest_port, size=struct.unpack('! H H 2x H', data[:8])
    return src_port, dest_port,size,data[8:]
 
# the public network interface
HOST = gethostbyname(gethostname()) 
# create a raw socket and bind it to the public interface
s = socket.socket(AF_INET, SOCK_RAW, IPPROTO_IP)
s.bind(("192.168.0.19", 0))
# Include IP headers
s.setsockopt(IPPROTO_IP, IP_HDRINCL, 1)

s.ioctl(SIO_RCVALL, RCVALL_ON)


while True:
    data, addr= s.recvfrom(65535)
    #data = receiveData(s)
    print()
    print('------------------------------------------------------------')
    print('CALCINA SHARON ALEJANDRA  --------------  CI: 8303911 LP')
    print('------------------------------------------------------------/n')

    print ("\n\n Ethernet Header")
    dest_mac, src_mac, eth_proto, info = ethernet_frame(data)
    print(TAB_1+'Destination: {}, Source:{}, Protocol: {}'.format(dest_mac,src_mac,eth_proto))

        # get the IP header (the first 20 bytes) and unpack them
        # B - unsigned char (1)
        # H - unsigned short (2)
        # s - string
    unpackedData = struct.unpack('!BBHHHBBH4s4s' , data[:20])
        
    version_IHL = unpackedData[0]
    version = version_IHL >> 4                  # version of the IP
    IHL = version_IHL & 0xF                     # internet header length
    TOS = unpackedData[1]                       # type of service
    totalLength = unpackedData[2]
    ID = unpackedData[3]                        # identification
    flags = unpackedData[4]
    fragmentOffset = unpackedData[4] & 0x1FFF
    TTL = unpackedData[5]                       # time to live
    protocolNr = unpackedData[6]
    checksum = unpackedData[7]
    sourceAddress = inet_ntoa(unpackedData[8])
    destinationAddress = inet_ntoa(unpackedData[9])
    
    print ("An IP packet with the size %i was captured." % (unpackedData[2]))
    print(' IP HEADER ')
    #print ("Raw data: " , data)
    #print ("\nParsed data")
    print ("Version:\t\t" + str(version))
    print ("Header Length:\t\t" + str(IHL*4) + " bytes")
    print ("Type of Service:\t" + getTOS(TOS))
    print ("Length:\t\t\t" + str(totalLength))
    print ("ID:\t\t\t" + str(hex(ID)) + " (" + str(ID) + ")")
    print ("Flags:\t\t\t" + getFlags(flags))
    print ("Fragment offset:\t" + str(fragmentOffset))
    print ("TTL:\t\t\t" + str(TTL))
    print ("Protocol:\t\t", getProtocol(protocolNr))
    print ("Checksum:\t\t" + str(checksum))
    print ("Source:\t\t\t" + sourceAddress)
    print ("Destination:\t\t" + destinationAddress)
    #print ("Payload:\n", data[20:])
    # disabled promiscuous mode
    #s.ioctl(SIO_RCVALL, RCVALL_OFF)

    #ICMP
    if protocolNr==1:
        icmp_type,code,checksum,data=icmp_packet(data)
        print(TAB_1+'ICMP packet', )
        print(TAB_2+'Type: {}, Code: {}, Checksum: {}'.format(icmp_type, code, checksum))
        print(TAB_2+'Data: ')
        print(format_multi_line(DATA_TAB_3, data))

    #TCP
    elif protocolNr==6:
        src_port, dest_port, sequence, acknowledgement, flags_urg, flags_ack, flags_psh,flags_rst,flags_syn,flags_fin,data_offset,window,checksum,urgent,data=tcp_segment(data)
        print(TAB_1+'TCP Segment: ')
        print(TAB_2+'Source Port: {}, Deastination Port: {}'.format(src_port,dest_port))
        print(TAB_2+'Sequence: {}, Acknowledgement: {}'.format(sequence,acknowledgement))
        print(TAB_2+'Flags: ')
        print(TAB_3+'URG: {}, ACK: {}, PSH:{}, RST: {}, SYN:{}, FIN:{}'.format(flags_urg, flags_ack,flags_psh, flags_rst,flags_syn,flags_fin))
        print(TAB_2+'Data Offset: {},Window: {}, checksum: {}, Urgent Pointer: {}'.format(data_offset,window,checksum,urgent))
        #print(TAB_2+'Data: ')
        #print(format_multi_line(DATA_TAB_3, data))
        print()        
    # UDP 
    elif protocolNr==17:
        src_port, dest_port,length,data=udp_segment(data)
        print(TAB_1+'UDP Segment: ')
        print(TAB_2+'Source Port: {}, Destination Port: {}, Length: {}'.format(src_port,dest_port,length))